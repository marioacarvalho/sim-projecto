<?php
	
session_start();
include("db_helper.php");
include("session_helper.php");
include("user_helper.php");

if (!isLoggedIn() || !isAdmin()) {
	redirectTo('index.php');
}
            	
?>

<!DOCTYPE html>
<html>
<?php require("header.php") ?>
<style type="text/css">

</style>
<body>

<?php require("menu.php") ?>
<div class="container">
<h1>Novo Utilizador</h1>
	<?php printCreateUser(); ?>

</div>
</body>
</html>
