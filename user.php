<?php

session_start();
include("db_helper.php");
include("session_helper.php");
include("user_helper.php");
include("friends_helper.php");

$user_id = null;

if (isset($_GET['id'])) {
	$user_id = $_GET["id"];
}

$isLoggedIn = isLoggedIn();

if ($user_id == null) {
	redirectTo('index.php');
}

$currentUserID = getCurrentUserID();

$isCurrentUser = isCurrentUser($user_id);

$isAdmin = isAdmin();

$isFriend = isFriend($currentUserID, $user_id);

$user = getUser($user_id);
?>

<!DOCTYPE html>
<html>
<?php require("header.php") ?>
<style type="text/css">

</style>
<body>

	<?php require("menu.php") ?>
	<div class="container">



		<?php printUser($user, $isAdmin || $isCurrentUser, false, $isLoggedIn, $isFriend); ?>

		<?php if ($isLoggedIn && !$isCurrentUser && !$isFriend) { ?>
			<?php $inviteURL = inviteUser($user_id); ?>
			<br><br>
			<form class="form-inline" action="friends/create.php" method='post'>
				<input type='hidden' name='invited_id' value='<?php echo $user_id; ?>'>
				<button type="submit" class="btn btn-primary">Adicionar amigo</button>
			</form>
			<?php } ?>

		<?php if ($isCurrentUser) { 

			$requests = getFriendsRequest($currentUserID);
			printFriendsRequests($requests);
			$friends = getFriendsList($currentUserID);
			printFriends($friends);
		} ?>
		</div>
	</body>
	</html>
