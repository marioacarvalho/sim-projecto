<?php

function getDBH()
{
	try {
		$user = "root";
		$pass = "root";
		$dbh = new PDO('mysql:host=localhost;dbname=simdb', $user, $pass);
		return $dbh;
	} catch (PDOException $e) {
		print "Error!: " . $e->getMessage() . "<br/>";
		die();
	}	#
}


function getUserLogin($username = '', $password = '') {
	$myDBH = getDBH();
	$stmt = $myDBH->prepare("SELECT * FROM users WHERE username = :username AND password = :password LIMIT 1");
	$stmt->bindParam(':username', $username);
	$stmt->bindParam(':password', $password);

	if ($stmt->execute()) {
		if ($row = $stmt->fetch()) {
			return $row;

		}
	} else {
		return $null;
	}
}

function getFriendsRequest($userID) {
	$myDBH = getDBH();

	$friends_array = array();

	$stmt = $myDBH->prepare("SELECT * FROM friends INNER JOIN users ON friends.userid_from = users.id WHERE userid_to = :userid_to AND status = 0");
	$stmt->bindParam(':userid_to', $userID);

	if ($stmt->execute()) {
		while ($row = $stmt->fetch()) {
			array_push($friends_array, $row);

		}
	} 

	return $friends_array;
}

function getFriendsList($userID) {
	$myDBH = getDBH();

	$friends_array = array();

	$stmt = $myDBH->prepare("SELECT * FROM friends INNER JOIN users ON friends.userid_to = users.id WHERE userid_from = :userid_from AND status = 1");
	$stmt->bindParam(':userid_from', $userID);

	if ($stmt->execute()) {
		while ($row = $stmt->fetch()) {
			array_push($friends_array, $row);

		}
	} 

	$stmt2 = $myDBH->prepare("SELECT * FROM friends INNER JOIN users ON friends.userid_from = users.id WHERE userid_to = :userid_to AND status = 1");
	$stmt2->bindParam(':userid_to', $userID);

	if ($stmt2->execute()) {
		while ($row2 = $stmt2->fetch()) {
			array_push($friends_array, $row2);
		}
	} 

	return $friends_array;

}

function isFriend($currentUserID, $userID) {
	
	$myDBH = getDBH();

	$friends_array = array();

	$stmt = $myDBH->prepare("SELECT count(*) FROM friends WHERE (userid_from = :userid_from_one AND userid_to = :userid_to_one AND status = 1) OR (userid_from = :userid_from_two AND userid_to = :userid_to_two AND status = 1) LIMIT 1");
	$stmt->bindParam(':userid_from_one', $currentUserID);
	$stmt->bindParam(':userid_to_one', $userID);
	$stmt->bindParam(':userid_from_two', $userID);
	$stmt->bindParam(':userid_to_two', $currentUserID);

	if ($stmt->execute()) {
		if ($row = $stmt->fetch()) {
			if ($row[0] > 0) {
				return true;
			} else {
				return false;
			}

		}
		
	} 


}

function getUser($userID = '') {
	$myDBH = getDBH();

	$user_id = $userID;


	$stmt = $myDBH->prepare("SELECT * FROM users WHERE id = :id LIMIT 1");
	$stmt->bindParam(':id', $user_id);

	if ($stmt->execute()) {
		while ($row = $stmt->fetch()) {
			return $row;

		}
	} else {
		return null;
	}


	$myDBH = null;
}



