#Relatório de trabalho
###Mário Carvalho - N. 21601097

##Convenções

- Neste projecto foram usadas as convenções faladas na aulas tais como:
	- funções com nome em camelCase;
	- variaveis explicitas e com underscores;
	- Metodos POST para criar/editar informação;
	- Metodos GET para obter informação; 
	- Codigo reutilizado o melhor possivel;
- Neste projecto também foi usado `Bootstrap` para melhorar um pouco o aspecto visual.
	
##Organização de ficheiros

- Existem 3 tipos de ficheiros
	- Views Normais: Aqui estão os pedidos GET normais que podem ser identificados pelos nomes das paginas
		- user.php - Detalhe de um user
		- users.php - Todos os users
		- edit_user.php - Editar user
		- new_user.php - Criar novo user
		- menu.php - Reutilização de código para menu superior
		- head.php - Reutilização de código para ```<head>```
	- Helpers:
		- db_helper.php - ajuda aos pedidos à Base de Dados
		- users_helper.php - ajuda à reutilização de código para imprimir users
		- friends_helper.php - ajuda à reutilização de código para as amizades
		- session_helper.php - metodos auxiliares para a sessão do utilizador
	- Ficheiros para recepção de forms POST:
		- users
			- destroy.php
			- create.php
			- edit.php
			- logout.php
			- login.php
		- friends
			- create.php
			- accept.php 
		
##Base de Dados
- A Base de dados é constituida por 2 tabelas com os seguintes atributos:
 - users
 	- id (sequencial)
 	- username
 	- password
 	- country
 	- city
 	- job
 	- image_url
 	- is_admin 
 - friends
 	- id (sequencial)
 	- userid_from
 	- userid_to
 	- status (estados da amizade)
 		- 0: Pedido de amizade Pendente
 		- 1: Pedido de amizade aceite
 		
##Outras considerações 
- A sessão do utilizador é guardada numa cookie com a informação do `id` do utilizador logado

- O script para criação da base de dados está em `db/script.sql`

- A informação para o `PDO` aceder à base de dados local deve ser alterada no ficheiro `db_helper.php - pesquisar por root`
 