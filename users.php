<?php
	
session_start();
include("db_helper.php");
include("session_helper.php");
include("user_helper.php");

$user_id = getCurrentUserID();
$isCurrentUser = isCurrentUser($user_id);
$isAdmin = isAdmin();

?>

<!DOCTYPE html>
<html>
<?php require("header.php") ?>
<style type="text/css">

</style>
<body>

<?php require("menu.php") ?>
<div class="container">

	<?php if (isAdmin()) {
		printAllUsersForAdmin();
	} else {
		printAllUsersPublic();

	} ?>
</div>
</body>
</html>
