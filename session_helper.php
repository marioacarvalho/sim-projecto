<?php


function createSession($user) {
	session_start();

	$_SESSION['user_id'] = $user['id'];
}

function closeSession() {
	$helper = array_keys($_SESSION);
	foreach ($helper as $key){
    	unset($_SESSION[$key]);
    }
	redirectTo('../index.php');

}


function isLoggedIn() {

	if ($_SESSION && $_SESSION['user_id']) {
		return true;
	} 
	return false;
}

function isAdmin() {
	if (isLoggedIn()) {
		if ($user = getUser($_SESSION['user_id'])){
			return strcmp($user['is_admin'], '1') == 0;
		}
	}
	return false; 
}

function getCurrentUsername() {

	if (isLoggedIn()) {
		if ($user = getUser($_SESSION['user_id'])){
			return $user['name'];
		}
	} 
}

function getCurrentUserID() {

	if (isLoggedIn()) {
		return $_SESSION['user_id'];
	} 
}

function isCurrentUser($user_id) {
	if ($user_id && isLoggedIn() && strcmp($user_id, $_SESSION['user_id']) == 0) {
		return true;
	}
	return false;
}

function redirectTo($url){
	if (headers_sent()){
		die('<script type="text/javascript">window.location.href="' . $url . '";</script>');
	}else{
		header('Location: ' . $url);
		die();
	}    
}

?>