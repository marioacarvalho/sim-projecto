<?php

// RELATION STATUS

// 0 invited
// 1 accepted
// 2 rejected
// 3 destroyed

function inviteUser($userID) {
	return 'friends/create.php?invited_id='.$userID;
}




function printFriendsRequests($friendsRequest) {
	if (count($friendsRequest) == 0) {
		return;
	}
	echo "<h1>Pedidos de amizade:</h1>";
	echo "<table>";
	echo "<tr>";
	echo "<th></th>";
	echo "<th></th>";
	echo "</tr>";
	foreach ($friendsRequest as $friend) {
		echo "<tr>";
		echo "<td>".$friend['name']." quer ser teu amigo! </td>";

		echo "<td><a href='friends/accept.php?inviter_id=".$friend['userid_from']."'>Aceitar</a></td>";
	}
	echo "</table>";
}

function printFriends($friends) {
	if (count($friends) == 0) {
		return;
	}
	echo "<h1>Amigos:</h1>";
	echo "<table>";
	echo "<tr>";
	echo "<th>Nome</th>";
	echo "<th></th>";
	echo "</tr>";
	foreach ($friends as $friend) {
		echo "<tr>";
		echo "<td>".$friend['name']."</td>";
		echo "<td><a href='user.php?id=".$friend['id']."'>Detalhe</a></td>";
		echo "</tr>";
	}
	echo "</table>";
}

?>