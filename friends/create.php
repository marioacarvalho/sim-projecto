<?php
	
session_start();
include("../db_helper.php");
include("../session_helper.php");

$user_id = $_POST["invited_id"];
$current_userId = getCurrentUserID();
$relation_status = 0;

//var_dump($current_userId);

$myDBH = getDBH();

$stmt = $myDBH->prepare("INSERT INTO friends (userid_from, userid_to, status) VALUES (:userid_from, :userid_to, :status)");
$stmt->bindParam(':userid_from', $current_userId);
$stmt->bindParam(':userid_to', $user_id);
$stmt->bindParam(':status', $relation_status);

$stmt->execute();

header('Location: ../users.php');
?>