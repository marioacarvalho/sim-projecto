<?php
	
session_start();
include("../db_helper.php");
include("../session_helper.php");

$user_id = $_GET["inviter_id"];
$current_userId = getCurrentUserID();
$relation_status = 0;

//var_dump($current_userId);

$myDBH = getDBH();

$stmt = $myDBH->prepare("UPDATE friends SET status = 1 WHERE userid_from = :userid_from AND userid_to = :userid_to");
$stmt->bindParam(':userid_from', $user_id);
$stmt->bindParam(':userid_to', $current_userId);


$stmt->execute();

header('Location: ../user.php?id='.$current_userId);
?>