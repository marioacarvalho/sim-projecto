<?php

function printUser($user, $current_user, $for_edit, $logged_in = true, $isfriend = false) {

		$readonly = $for_edit ? '' : 'readonly';

	if (!$logged_in) {
		echo "<h1><img src='".$user['image_url']."' height='150' width='150'></h1>";
		echo "Nome: <input type='text' name='username' value='".$user["name"]."' ".$readonly."><br>";
		return;
	}

	if ($readonly) {
		echo "<h1><img src='".$user['image_url']."' height='150' width='150'></h1>";
	}

	echo "<form action='users/edit.php' method='POST'>";

	
	echo "Nome: <input type='text' name='name' value='".$user["name"]."' ".$readonly." required><br>";
	if ($logged_in && ($current_user || $isfriend)) {
		echo "Utilizador: <input type='text' name='username' value='".$user["username"]."' ".$readonly."><br>";
		if ($for_edit) {
		echo "Password: <input type='text' name='password' value='".$user["password"]."' ".$readonly."><br>";
	}
		echo "Pais: <input type='text' name='country' value='".$user["country"]."' ".$readonly."><br>";
		echo "Cidade: <input type='text' name='city' value='".$user["city"]."' ".$readonly."><br>";
		echo "Profissão: <input type='text' name='job' value='".$user["job"]."' ".$readonly."><br>";

		if ($for_edit) {
			echo "Fotografia: <input type='text' name='picture_url' value='".$user["image_url"]."' ".$readonly." required><br><br>";
			echo "<input type='hidden' name='id' value=".$user['id'].">";
			echo "<input type='submit' name='submit' value='Submeter'>";
			echo "</form>";
		} else {
					echo "</form>";
			if ($current_user) {
			echo "<br><a href='edit_user.php?user_id=".$user["id"]."'>Editar</a>";
			}
		}
	} else {
		echo "<p>Não é possivel mostrar mais informação pois não há autorização de amizade.</p>";
		echo "</form>";
	}
	


}

function printCreateUser() {
	echo "<form action='users/new.php' method='POST'>";
	echo "Utilizador: <input type='text' name='username'><br>";
	echo "Password: <input type='text' name='password'><br>";
	echo "Nome: <input type='text' name='name' required><br>";
	echo "Pais: <input type='text' name='country'><br>";
	echo "Cidade: <input type='text' name='city'><br>";
	echo "Profissão: <input type='text' name='job'><br>";
	echo "Fotografia: <input type='text' name='picture_url' required><br>";
	echo "Administrador: <input type='checkbox' name='is_admin' value='1'><br><br>";
	echo "<input type='submit' name='submit' value='Criar'>";
	echo "</form>";
}

function printAllUsersForAdmin() {
	$myDBH = getDBH();

	echo "<table>";
	echo "<tr>";
	echo "<th>ID</th>";
	echo "<th>Utilizador</th>";
	echo "<th>Nome</th>";
	echo "<th>Pais</th>";
	echo "<th>Cidade</th>";
	echo "<th>Profissão</th>";
	echo "<th>Fotografia</th>";
	echo "<th>Admin</th>";
	echo "<th></th>";
	echo "</tr>";

	$stmt = $myDBH->prepare("SELECT * FROM users");
	if ($stmt->execute()) {
		while ($row = $stmt->fetch()) {
			echo "<tr>";
			echo "<td>".$row['id']."</td>";
			echo "<td>".$row['username']."</td>";
			echo "<td>".$row['name']."</td>";
			echo "<td>".$row['country']."</td>";
			echo "<td>".$row['city']."</td>";
			echo "<td>".$row['job']."</td>";
			echo "<td><img src='".$row['image_url']."' alt='Smiley face' height='50' width='50'></td>";
			echo "<td><a href='user.php?id=".$row['id']."'>Detalhe</a></td>";
			echo "<td><form action='users/destroy.php' method='post'><input type='hidden' name='id' value=".$row['id']."><input type='submit' name='submit' value='Apagar'></form></td>";
			echo "</tr>";
		}
	}
	echo "</table>";

	$myDBH = null;
}

function printAllUsersPublic () {
	$myDBH = getDBH();

	echo "<table>";
	echo "<tr>";
	echo "<th>Nome</th>";
	echo "<th>Fotografia</th>";
	echo "<th></th>";
	echo "</tr>";

	$stmt = $myDBH->prepare("SELECT * FROM users");
	if ($stmt->execute()) {
		while ($row = $stmt->fetch()) {
			echo "<tr>";
			echo "<td>".$row['name']."</td>";
			echo "<td><img src='".$row['image_url']."' alt='Smiley face' height='50' width='50'></td>";
			echo "<td><a href='user.php?id=".$row['id']."'>Detalhe</a></td>";
			echo "</tr>";
		}
	}
	echo "</table>";

	$myDBH = null;
}
?>