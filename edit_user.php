<?php
	
session_start();
include("db_helper.php");
include("session_helper.php");
include("user_helper.php");

$user_id = $_GET['user_id'];

if ($user_id == null) {
	redirectTo('index.php');
}

$user = getUser($user_id);

?>

<!DOCTYPE html>
<html>
<?php require("header.php") ?>
<style type="text/css">

</style>
<body>

<?php require("menu.php") ?>
<div class="container">
<h1>Editar</h1>
	<?php printUser($user, isAdmin() || isCurrentUser(getCurrentUserID()), true); ?>
</div>
</body>
</html>
