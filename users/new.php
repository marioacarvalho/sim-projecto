<?php

require("../db_helper.php");

//var_dump($_REQUEST);


$myDBH = getDBH();

$username = $_POST["username"];
$password = $_POST["password"];
$name = $_POST["name"];
$country = $_POST["country"];
$city = $_POST["city"];
$picture_url = $_POST["picture_url"];
$job = $_POST["job"];
$is_admin = empty($_POST["is_admin"]) ? 0 : 1;

$stmt = $myDBH->prepare("INSERT INTO users (username, password, name, country, city, image_url, job, is_admin) VALUES (:username, :password, :name, :country, :city, :image_url, :job, :is_admin)");
$stmt->bindParam(':username', $username);
$stmt->bindParam(':password', $password);
$stmt->bindParam(':name', $name);
$stmt->bindParam(':country', $country);
$stmt->bindParam(':city', $city);
$stmt->bindParam(':image_url', $picture_url);
$stmt->bindParam(':job', $job);
$stmt->bindParam(':is_admin', $is_admin,PDO::PARAM_INT);
if (strcmp($is_admin, '1') == 0) {
	
} else {
		$stmt->bindParam(':is_admin', $is_admin, PDO::PARAM_INT);

}

$stmt->execute();

header('Location: ../index.php');
//redirectTo("../index.php");
?>
