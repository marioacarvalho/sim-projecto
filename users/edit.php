<?php

require("../db_helper.php");

//var_dump($_REQUEST);


$myDBH = getDBH();

$user_id = $_POST["id"];
$username = $_POST["username"];
$password = $_POST["password"];
$name = $_POST["name"];
$country = $_POST["country"];
$city = $_POST["city"];
$picture_url = $_POST["picture_url"];
$job = $_POST["job"];

$stmt = $myDBH->prepare("UPDATE users SET username = :username, password = :password, name = :name, country = :country, city = :city, image_url = :image_url, job = :job WHERE id = :id");
$stmt->bindParam(':username', $username);
$stmt->bindParam(':password', $password);
$stmt->bindParam(':name', $name);
$stmt->bindParam(':country', $country);
$stmt->bindParam(':city', $city);
$stmt->bindParam(':image_url', $picture_url);
$stmt->bindParam(':job', $job);
$stmt->bindParam(':id', $user_id,PDO::PARAM_INT);


$stmt->execute();

header('Location: ../user.php?id='.$user_id);
//redirectTo("../index.php");
?>
