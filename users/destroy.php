<?php

require("../db_helper.php");

$myDBH = getDBH();

$id = $_POST["id"];

$stmt = $myDBH->prepare("DELETE FROM users WHERE id = :id");
$stmt->bindParam(':id', $id);

$stmt->execute();

header('Location:../users.php');