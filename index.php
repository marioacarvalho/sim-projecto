<?php
	
session_start();
include("db_helper.php");
include("session_helper.php");

?>

<!DOCTYPE html>
<html>
<?php require("header.php") ?>
<style type="text/css">

</style>
<body>

<?php require("menu.php") ?>
<div class="container">
	<?php if (!isLoggedIn()) { ?>
		<h1>Please Login</h1>
  		<form action="users/login.php" method="post">
			<span>Username:</span> <input type="text" name="username" required><br>
			<span>Password:</span> <input type="text" name="password" required><br>
			<input type="submit">
		</form>
	<?php } else { ?>
		<p> Hello, <?php echo getCurrentUsername(); ?> </p>
	<?php } ?>

</div>
</body>
</html>